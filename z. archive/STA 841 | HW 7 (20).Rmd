---
title: "STA 841 | Homework 7"
author: "Ricardo Batista (rb313)"
date: "3/6/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
source("../z. boot/startup.R")
```

# Simulation

```{r sim_setup, echo = TRUE, fig.align = "center", message = FALSE, warning = FALSE, cache = FALSE}

# Simulation parameters
EFF_B <- c(0.25, 0.50, 0.75) # 0.10
N     <- c(100, 500) # 1000, 5000
p     <- 20
PRIOR <- c("horse", "lasso")

# Jags setup
BURN_IN <- 10000; SAMP_NUM <- 10000

```

```{r sim, eval = FALSE, echo = TRUE, fig.align = "center", message = FALSE, warning = FALSE, cache = FALSE}

stats_sim <- NULL 

for (n in N) {
  
  # Produce covariates
  X <- mvrnorm(n, mu = rep(0, p), Sigma = diag(p))
  
  for (eff_b in EFF_B) {
    
    # Betas
    num_eff_b <- p*eff_b
    beta_0 <- 1
    B <- sample(rep(c(1, 0), c(num_eff_b, p - num_eff_b)), p, replace = FALSE)
    
    # Response
    t  <- beta_0 + X%*%B
    pr <- 1/(1 + exp(-t))
    y  <- rbinom(n, 1, pr)
    
    for (prior in PRIOR) {
      
      print(c(n, eff_b, prior))
      
      # Get performance stats
      stats <- get_stats_wrapper(prior, X, y, eff_b, BURN_IN, SAMP_NUM, B)
      stats <- data.frame(n = n, eff_b = eff_b, prior = prior, as.list(stats))
      stats_sim <- rbind(stats_sim, stats)
      
    }
    
  }
  
}

write.csv(stats_sim, "stats_sim.csv", row.names = FALSE)

```


```{r sim_plots, echo = TRUE, fig.align = "center", message = FALSE, warning = FALSE, cache = TRUE}

stats_sim <- read.csv("stats_sim.csv")

# Performance results
horse_table <- stats_sim %>% filter(prior == "horse") %>% dplyr::select(-prior)
kable(horse_table,
      caption = paste("MSE of point estimates and average 95pct CI width for the", "$\\beta$",
                      "'s (horseshoe)"),
                     booktabs = T, digits = 3) %>%
          kable_styling(latex_options = "hold_position")

lasso_table <- stats_sim %>% filter(prior == "lasso") %>% dplyr::select(-prior)
kable(lasso_table,
      caption = paste("MSE of point estimates and average 95pct CI width for the", "$\\beta$",
                      "'s (double exponential)"),
                     booktabs = T, digits = 3) %>%
          kable_styling(latex_options = "hold_position")


# Example
eff_b <- 0.25
n     <- 500
p     <- 20
prior <- "horse"

# Covariates
X <- mvrnorm(n, mu = rep(0, p), Sigma = diag(p))

# Betas
num_eff_b <- p*eff_b
beta_0 <- 1
B <- sample(rep(c(1, 0), c(num_eff_b, p - num_eff_b)), p, replace = FALSE)

# Response
t  <- beta_0 + X%*%B
pr <- 1/(1 + exp(-t))
y  <- rbinom(n, 1, pr)
X <- mvrnorm(n, mu = rep(0, p), Sigma = diag(p))

samp <- get_sim_horse(X, y, 0.25, 1000, 1000)
get_boxplot_sim(samp)
```

&nbsp;&nbsp;&nbsp;&nbsp; Notice $\beta = $ `r print(B)`  

&nbsp;&nbsp;&nbsp;&nbsp; As expected, we see more accurate estimates for beta -- judging by MSE and CI interval -- the sparser $\beta$ is and the more observations we work with.  

## Correlation impact | Example

```{r sim_plots, echo = TRUE, fig.align = "center", message = FALSE, warning = FALSE, cache = TRUE}

# Example
eff_b <- 0.25
n     <- 500
p     <- 20
prior <- "horse"

# Covariates
X <- mvrnorm(n, mu = rep(0, p), Sigma = diag(p) + 0.1)

# Betas
num_eff_b <- p*eff_b
beta_0 <- 1
B <- sample(rep(c(1, 0), c(num_eff_b, p - num_eff_b)), p, replace = FALSE)

# Response
t  <- beta_0 + X%*%B
pr <- 1/(1 + exp(-t))
y  <- rbinom(n, 1, pr)
X <- mvrnorm(n, mu = rep(0, p), Sigma = diag(p))

samp <- get_sim_horse(X, y, 0.25, 1000, 1000)
get_boxplot_sim(samp)
```

Notice the covariance matrix is no longer diagonal. Notice the results are particularly underwhelming -- this is only because due to time contraints I was only able to run the chain for burn_in and samps equal to 1000! However, the model behaves well when using 10,000.  

## Alpha impact | Example

```{r sim_plots, echo = TRUE, fig.align = "center", message = FALSE, warning = FALSE, cache = TRUE}

# Example
eff_b <- 0.25
n     <- 500
p     <- 20
prior <- "horse"

# Covariates
X <- mvrnorm(n, mu = rep(0, p), Sigma = diag(p))

# Betas
num_eff_b <- p*eff_b
beta_0 <- 5
B <- sample(rep(c(1, 0), c(num_eff_b, p - num_eff_b)), p, replace = FALSE)

# Response
t  <- beta_0 + X%*%B
pr <- 1/(1 + exp(-t))
y  <- rbinom(n, 1, pr)
X <- mvrnorm(n, mu = rep(0, p), Sigma = diag(p))

samp <- get_sim_horse(X, y, 0.25, 1000, 1000)
get_boxplot_sim(samp)

```

Notice the intercept is now 5. Same as above, I was only able to run few iterations and burn-in.  

# Empirical results

```{r emp_results, echo = TRUE, fig.align = "center", message = FALSE, warning = FALSE, cache = TRUE}

samp <- get_emp_horse(diabetes, BURN_IN, SAMP_NUM)
get_boxplot_emp(samp)

samp <- get_emp_lasso(diabetes, BURN_IN, SAMP_NUM)
get_boxplot_emp(samp)

```

\pagebreak

# Appendix

&nbsp;&nbsp;&nbsp;&nbsp; The following pages contain all data and functions referred to in the code above. Ideally, we would run use 

```{r, echo = FALSE, message = FALSE, warning = FALSE, fig.align = "center", fig.width = 4, fig.height = 3}
plot_directory()
```

#### startup.R
```{r, code = readLines("../z. boot/startup.R"), eval = FALSE}
```

#### Data/data.R
```{r, code = readLines("../z. boot/Data/data.R"), eval = FALSE}
```

#### Functions/get_sim_horse.R
```{r, code = readLines("../z. boot/Functions/get_sim_horse.R"), eval = FALSE}
```

#### Functions/get_sim_lasso.R
```{r, code = readLines("../z. boot/Functions/get_sim_lasso.R"), eval = FALSE}
```

#### Functions/get_stats.R
```{r, code = readLines("../z. boot/Functions/get_stats.R"), eval = FALSE}
```

#### Functions/get_stats_wrapper.R
```{r, code = readLines("../z. boot/Functions/get_stats_wrapper.R"), eval = FALSE}
```

#### Functions/get_boxplot_sim.R
```{r, code = readLines("../z. boot/Functions/get_boxplot_sim.R"), eval = FALSE}
```


#### Functions/get_emp_horse.R
```{r, code = readLines("../z. boot/Functions/get_emp_horse.R"), eval = FALSE}
```

#### Functions/get_emp_lasso.R
```{r, code = readLines("../z. boot/Functions/get_emp_lasso.R"), eval = FALSE}
```

#### Functions/get_boxplot_emp.R
```{r, code = readLines("../z. boot/Functions/get_boxplot_emp.R"), eval = FALSE}
```

