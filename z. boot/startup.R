set.seed(2018)

lib <- c("R.utils", "plyr", "tidyverse", "gridExtra", "reshape2", "kableExtra", "grid",
         "pracma", "stringr", "MASS", "latex2exp", "data.tree", "ggpubr",
         "rmutil", "rjags")

noise <- lapply(lib, library, character.only = TRUE, quietly = TRUE, verbose = FALSE, 
                warn.conflicts = FALSE)

#### Source ####
sourceDirectory("z. boot/Data", modifiedOnly = FALSE)
sourceDirectory("z. boot/Functions", modifiedOnly = FALSE)
