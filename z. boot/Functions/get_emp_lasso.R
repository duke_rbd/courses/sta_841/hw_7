get_emp_lasso <- function(diabetes, BURN_IN, SAMP_NUM) {
  
  X <- dplyr::select(diabetes, -diabetes); p <- ncol(X)
  y <- diabetes$diabetes
  n <- nrow(diabetes)
  
  # Check
  mod_check <- glm(diabetes ~ ., data = diabetes, family = "binomial")
  y_hat <- round(predict(mod_check, type = "response"))
  var_y <- (1/(n - 1))*sum((y - y_hat)^2)
  
  
  # Lasso
  logistic_model_lasso <- "model{
  # Likelihood
  
  for(i in 1:n){
  Y[i] ~ dbern(q[i])
  logit(q[i]) <- beta0 +
  beta[1]*X[i,1] + beta[2]*X[i,2] + beta[3]*X[i,3] + beta[4]*X[i,4] +
  beta[5]*X[i,5] + beta[6]*X[i,6] + beta[7]*X[i,7]
  
  }
  
  # Priors
  lambda ~ dunif(0.001,10)
  
  ## Intercept
  beta0 ~ ddexp(0, lambda)
  
  ## Betas
  for(j in 1:p) {
  
  beta[j] ~ ddexp(0, lambda)
  
  }
  
}"
dat   <- list(Y = y, X = X, n = n, p = p)
model <- jags.model(textConnection(logistic_model_lasso), data = dat, n.chains = 1, 
                    quiet = TRUE)
update(model, BURN_IN, progress.bar = "text")
samp <- coda.samples(model, variable.names = c("beta0", "beta"),
                     n.iter = SAMP_NUM, progress.bar = "text") # Maybe reset to 20000

return(samp)
  
}
