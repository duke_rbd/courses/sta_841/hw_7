get_boxplot_sim <- function(samp) {
  
  samps_df <- do.call(rbind.data.frame, samp)
  ggplot(stack(samps_df), aes(x = ind, y = values)) + geom_boxplot() +
    theme(axis.text.x = element_text(angle = 90, hjust = 1))
  
}
